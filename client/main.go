package main

import (
	"bufio"
	"os"
	"fmt"
	"strings"
	"time"
	"gitlab.com/localtoast/loaf"
	zmq "github.com/pebbe/zmq4"
)

func main() {
	//Hostname == localhost right now
	hostname := "127.0.0.1"
	//This is the start of zmq contexts
	ctx, err := zmq.NewContext()
	if err != nil {
		panic(err)
	}
	pinger, err := ctx.NewSocket(zmq.REQ)
	pinger.Connect("tcp://"+hostname+":4000")
	fmt.Println(pinger)
	_, _, dough, _ := loaf.DoughMax()
	screenX, screenY := 150, 15
	loaf.Oven(dough, "x", screenX, screenY)
	loaf.Flat("*", dough)

	var output []string
	loginPos := loaf.MoveTo(screenX/2, 7)
	loginSpacePos := loaf.MoveTo(screenX/2, 8)
	login := loginPos+"CONNECTED TO"
	loginSpace := loginSpacePos+hostname
	loginSpace = loaf.Dye256(loginSpace, "255", "150", "100", true, false)
	login = loaf.Dye256(login, "125", "250", "100", true, false)
//	dough = loaf.CopyToast(login, 75, 7, 1, dough)
//	dough = loaf.CopyToast(loginSpace, 75, 8, 1, dough)
	//loaf.Toast(dough, "", "")
	output = append(output, login)
	output = append(output, loginSpace)
	for i := 0;i < len(output);i++ {
		output[i] = loaf.Dye256(output[i], "0", "0", "0", false, true)
		fmt.Printf(output[i])
		end := loaf.MoveTo(75, 9)
		fmt.Printf(end)
	}

	for true {

		reader := bufio.NewReader(os.Stdin)
		value, err := reader.ReadString('\n')
		if err != nil {
			panic(err)
		}
		if strings.Contains(value, "quit()") {
			fmt.Println("Have a crashtastic day!")
			os.Exit(0)
		}
		loaf.Flat("_", dough)
		output = output[:0]
		conn := loaf.Dye256("<**CONNECTED**>", "150", "255", "100", true, false)
		output = append(output, conn)
		//redraw the screen
		for i := 0;i < len(output);i++ {
			output[i] = loaf.Dye256(output[i], "0", "0", "0", false, true)
			end := loaf.MoveTo(1, 1)
			fmt.Printf(end)
			fmt.Printf(output[i])
		}

		pinger.Send(value, 0)
		pingTime := time.Now()
		message, err := pinger.Recv(0)
		if err != nil {
			panic(err)
		}
		pingRecv := time.Now()
		pingFinal := pingRecv.Sub(pingTime)
		messPos := loaf.MoveTo(75, 1)
		fmt.Printf(messPos)
		fmt.Printf("SERVER SAYS, "+message)
		pingPos := loaf.MoveTo(screenX-11, 1)
		fmt.Printf(pingPos)
		fmt.Printf(pingFinal.String())
		inputPos := loaf.MoveTo(1, screenY)
		inputBox := loaf.Dye256("             ", "200", "100", "100", false, true)
		fmt.Printf(inputPos+inputBox)
		fmt.Printf(inputPos)
	}


}
