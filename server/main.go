package main

import (
	"fmt"
	zmq "github.com/pebbe/zmq4"
)

func main() {
	//Hostname is localhost right now
	hostname := "127.0.0.1"
	//This is the start of zmq contexts
	ctx, err := zmq.NewContext()
	if err != nil {
		panic(err)
	}
	pinger, err := ctx.NewSocket(zmq.REP)
	pinger.Bind("tcp://"+hostname+":4000")
	fmt.Println(pinger)
	for {
		message, err := pinger.Recv(0)
		if err != nil {
			panic(err)
		}
		fmt.Println(message)
		pinger.Send("PONG", 0)
	}

}
