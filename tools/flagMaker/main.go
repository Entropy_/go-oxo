package main

import (
	"fmt"
	"strings"
	"image/jpeg"
	"os"
)

func main() {
	ascii := strings.Split(" -.-,-:-;-i-r-s-X-A-2-5-3-h-M-H-G-S-#-9-B-&-@", "-")
	//This takes the first argument to the call and opens it for flag-ifying
	file, err := os.Open(os.Args[1])
	defer file.Close()
	if err != nil {
		panic(err)
	}
	img, err := jpeg.Decode(file)
	if err != nil {
		panic(err)
	}
	bounds := img.Bounds()
	var newFlag []string
	for row := 0;row < bounds.Max.X;row += bounds.Max.X/32 {
		for col := 0;col < bounds.Max.Y;col += bounds.Max.Y/32 {
			oldPixel := img.At(col, row)
			r, g, b, _ := oldPixel.RGBA()
			lumosity := (0.299 * float64(r) + 0.587 * float64(g) + 0.114 * float64(b))
			newValue := int(lumosity/256)
			newPix := newValue / len(ascii)
			newFlag = append(newFlag, ascii[newPix])
		}
		newFlag = append(newFlag, "\n")
	}
	fmt.Println(newFlag)
	//Now write the string to a file
	fileOut, err := os.Create("flag.txt")
	defer fileOut.Close()
	if err != nil {
		panic(err)
	}
	for i := 0;i < len(newFlag);i++ {
		fileOut.WriteString(newFlag[i])
		fileOut.WriteString(" ")
	}
	fileOut.Sync()


}
